use std::env::args;
use std::ops::Add;
use serde::{Deserialize, Serialize};
use actix_web::body::MessageBody;
use dyncms::{content_types, run, DefaultPageTypes, PageContent, HttpRequest, HttpResponse};
use dyncms::pages::IntoPageContent;
use maud::{html, PreEscaped, DOCTYPE};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Page<T> {
    header: T,
    content: T,
    footer: T,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Raw(String);

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Composition<T>(Vec<T>);

impl<T: IntoPageContent + Clone> PageContent for Page<T> {
    fn respond(&mut self, req: HttpRequest) -> HttpResponse {
        let header_content = PreEscaped(self.header_string(&req));
        let main_content = PreEscaped(self.content_string(&req));
        let footer_content = PreEscaped(self.footer_string(&req));

        let page = html! {
            (DOCTYPE)
            body {
                header { (header_content) }
                main { (main_content) }
                footer { (footer_content) }
            }
        };

        HttpResponse::Ok().body(page.0)
    }
}

impl<T: IntoPageContent + Clone> Page<T> {
    pub fn header_string(&mut self, req: &HttpRequest) -> String {
        Self::response_to_string(self.header.clone().into_page_content().respond(req.clone()))
    }

    pub fn content_string(&mut self, req: &HttpRequest) -> String {
        Self::response_to_string(self.content.clone().into_page_content().respond(req.clone()))
    }

    pub fn footer_string(&mut self, req: &HttpRequest) -> String {
        Self::response_to_string(self.footer.clone().into_page_content().respond(req.clone()))
    }
}

impl<T> Page<T> {
    fn response_to_string(response: HttpResponse) -> String {
        if let Ok(bytes) = response.into_body().try_into_bytes() {
            return String::from(String::from_utf8_lossy(bytes.as_ref()));
        }

        String::new()
    }
}

impl PageContent for Raw {
    fn respond(&mut self, _: HttpRequest) -> HttpResponse {
        HttpResponse::Ok().body(self.0.clone())
    }
}

impl<T: IntoPageContent + Clone> PageContent for Composition<T> {
    fn respond(&mut self, req: HttpRequest) -> HttpResponse {
        let mut content = String::new();

        for component in &mut self.0 {
            let response = component.clone().into_page_content().respond(req.clone());

            if let Ok(bytes) = response.into_body().try_into_bytes() {
                content = content.add(String::from_utf8_lossy(bytes.as_ref()).as_ref());
            }
        }

        HttpResponse::Ok().body(content)
    }
}

content_types! {
    CmsPages extends DefaultPageTypes {
        Raw(Raw),
        Composition(Composition<Box<CmsPages>>),
        Page(Page<Box<CmsPages>>)
    }
}

#[dyncms::main]
async fn main() {
    println!("Running {} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));

    run::<CmsPages, _>(args()).await;
}
